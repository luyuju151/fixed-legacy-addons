# Fixed Legacy Addons for Waterfox

Personally fixed Firefox legacy addons for Waterfox 56.2.*.  
Uploaded here for backup purpose.  
These fixed addons are not signed and are experimental versions for anyone interested.  
The addons are provided "as is" without warranty of any kind, either express or implied. Use at your own risk.   

You can diff the decompressed xpi source codes with the origin to make sure the changes are safe.  

If you find these versions violate your rights, please contact me.  